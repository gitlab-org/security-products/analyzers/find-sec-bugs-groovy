# find-sec-bugs Groovy analyzer changelog

## v2.0.0
- Switch to new report syntax with `version` field

## v1.1.0
- Add `Scanner` property and deprecate `Tool`

## v1.0.0
- initial release
