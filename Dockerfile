FROM gradle

ENV FINDSECBUGS_VERSION=1.8.0

USER 0

# Install FindSecBugs
COPY fsb /fsb
RUN cd /fsb && \
  wget https://github.com/find-sec-bugs/find-sec-bugs/releases/download/version-${FINDSECBUGS_VERSION}/findsecbugs-cli-${FINDSECBUGS_VERSION}.zip && \
  unzip -n findsecbugs-cli-${FINDSECBUGS_VERSION}.zip

COPY analyzer /

USER 1000
ENTRYPOINT []
CMD ["/analyzer", "run"]
