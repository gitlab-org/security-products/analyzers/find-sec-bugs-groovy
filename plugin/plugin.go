package plugin

import (
	"os"
    "strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
	"path/filepath"
	"gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-groovy/builder"
)

var SearchedForGroovyFiles bool = false
var FoundGroovyFiles bool = false
var Builder builder.Builder = builder.DefaultBuilder()

func DetectGroovyFiles(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	// Ignore Gradle config files
	if info.Name() == "gradle" {
		return filepath.SkipDir
	}

	if FoundGroovyFiles {
		return filepath.SkipDir
	} else if strings.HasSuffix(info.Name(), ".groovy") {
		FoundGroovyFiles = true
		return filepath.SkipDir
	}

	return nil
}

func Match(path string, info os.FileInfo) (bool, error) {
	// First, do a search of our own for .groovy files, nothing can be decided
	// without that information.
	if !SearchedForGroovyFiles {
		filepath.Walk(path, DetectGroovyFiles)
		SearchedForGroovyFiles = true
	}

	if !FoundGroovyFiles {
		return false, nil
	}

	// Run the Builders' detection to see if some can build the project.
	for _, b := range builder.Builders{
		if b.Detect(info) {
			Builder = b
			return true, nil
		}
	}

	return false, nil
}

func init() {
	plugin.Register("find-sec-bugs-groovy", Match)
}
