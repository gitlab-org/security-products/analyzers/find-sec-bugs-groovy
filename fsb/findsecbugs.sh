#!/bin/sh

FSB_DIR=/fsb

PLUGIN=${FSB_DIR}/lib/findsecbugs-plugin-*.jar

java -cp "$FSB_DIR/lib/*" edu.umd.cs.findbugs.LaunchAppropriateUI -quiet -pluginList ${PLUGIN} $@
