package main

import (
	"io"
	"os"
	"os/exec"
	"github.com/urfave/cli"
	"github.com/logrusorgru/aurora"
	"fmt"
	"gitlab.com/gitlab-org/security-products/analyzers/find-sec-bugs-groovy/plugin"
	"path/filepath"
)

const flagBuild = "build"

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.BoolTFlag{
			Name:  flagBuild,
			Usage: "Build Groovy application. It's not needed if the code is already compiled.",
		},
	}
}

const (
	pathFSB              = "/fsb/findsecbugs.sh"
	pathOutput           = "/tmp/findSecurityBugs.xml"
)

// SourceFiles lists the java and groovy source file names, for use later in the convert step to filter out library vulnerabilities
var SourceFiles = make(map[string]bool)

func setupCmd(projectPath string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = projectPath
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	// Gather the list of source file names.
	filepath.Walk(projectPath, func(filePath string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		if filepath.Ext(filePath) == ".java" || filepath.Ext(filePath) == ".groovy" {
			SourceFiles[filepath.Base(filePath)] = true
		}
		return nil
	})

	fmt.Println(aurora.Sprintf(aurora.Green("Found %d Java and Groovy source files."), len(SourceFiles)))

	// Build Groovy source code if needed
	if c.BoolT(flagBuild) {
		// First try a static compilation since it allows findbugs to find
		// more vulnerabilities
		fmt.Println(aurora.Green("Building Groovy project with static compilation."))
		plugin.Builder.Configure(projectPath, true)

		if exitCode, err := plugin.Builder.Build(projectPath) ; exitCode != 0 {
			// It didn't work, so try a non static build
			fmt.Println(aurora.Green("Building failed, trying building without static compilation."))
			plugin.Builder.Configure(projectPath, false)

			if exitCode, _ := plugin.Builder.Build(projectPath) ; exitCode != 0 {
				fmt.Println(aurora.Sprintf(aurora.Red("Project couldn't be built: %s\n"), err.Error()))
			} else {
				fmt.Println(aurora.Green("Project built.\n"))
			}
		}
	}

	// Run findSecBugs task, even if the build failed
	// because they may have failed after successfully compiling the files.
	cmd := setupCmd(
		projectPath,
		exec.Command(
			pathFSB,
			"-exclude", "/fsb/exclude.xml",
			"-include", "/fsb/include.xml",
			"-effort:max", "-low",
			"-output", pathOutput,
			projectPath))

	if err := cmd.Run(); err != nil {
		fmt.Println(aurora.Sprintf(aurora.Red("FindSecBug analysis failed: %s\n"), err.Error()))
		return nil, err
	}

	fmt.Println(aurora.Sprintf(aurora.Green("FindSecBug analysis succeeded!\n")))
	return os.Open(pathOutput)
}
