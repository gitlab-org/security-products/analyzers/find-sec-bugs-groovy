package builder

import (
	"os"
	"path/filepath"
	"github.com/termie/go-shutil"
	"io"
	"os/exec"
	"syscall"
	"path"
)

const (
	pathGradle           = "/usr/bin/gradle"
	pathExtraBuildGradle = "/fsb/build.gradle"
)

type Builder struct {
	Name string
	Detect func(info os.FileInfo) bool // Function that recognise files that the builder can handle
	Configure func(projectPath string, static bool) error // Configures the project with or without static compilation
	Build func(projectPath string) (exitCode int, err error) // builds the project
}

func setupCmd(projectPath string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = projectPath
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd
}

// Run a command and get the exit code
func runCmd(cmd *exec.Cmd) (exitCode int, err error) {
	err = cmd.Run()

	if err != nil {
		switch err.(type) {
		case *exec.ExitError:
			// The command failed during its execution
			exitError := err.(*exec.ExitError)
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			exitCode = waitStatus.ExitStatus()
		default:
			// The command couldn't even be executed
			exitCode = 1
		}
	} else {
		// The command successfuly executed
		waitStatus := cmd.ProcessState.Sys().(syscall.WaitStatus)
		exitCode = waitStatus.ExitStatus()
	}

	return exitCode, err
}

// Configure a Gradle project to be statically compiled or not
func configureGraddleProject(projectPath string, static bool) error {
	original_build_file := filepath.Join(projectPath, "build.gradle")
	target := filepath.Join(projectPath, "build-configured.gradle")

	if static {
		// Copy original build file to a new one
		shutil.Copy(original_build_file, target, false)

		// Append extra configuration to it
		from, err := os.Open(pathExtraBuildGradle)
		if err != nil {
			return err
		}
		defer from.Close()

		to, err := os.OpenFile(target, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			return err
		}
		defer to.Close()

		if _, err := io.Copy(to, from); err != nil {
			return err
		}
	} else {
		// Just copy the build file without any change.
		shutil.Copy(original_build_file, target, false)
	}

	return nil
}

var Builders = []Builder {
	// The Gradlew builder will try to run the gradlew wrapper script to build the project
	Builder{
		Name: "Gradlew",
		Detect: func(info os.FileInfo) bool {
			return info.Name() == "gradlew"
		},
		Configure: func(path string, static bool) error {
			return configureGraddleProject(path, static)
		},
		Build: func(projectPath string) (exitCode int, err error) {
			cmd := setupCmd(projectPath, exec.Command(path.Join(projectPath, "gradlew"), "build", "-b", "build-configured.gradle"))
			exitCode, err = runCmd(cmd)
			return
		},
	},
	// The Gradle builder will try to run the Gradle installed in the image to build the project
	Builder{
		Name: "Gradle",
		Detect: func(info os.FileInfo) bool {
			return info.Name() == "build.gradle"
		},
		Configure: func(path string, static bool) error {
			return configureGraddleProject(path, static)
		},
		Build: func(projectPath string) (exitCode int, err error) {
			cmd := setupCmd(projectPath, exec.Command(pathGradle, "build", "-b", "build-configured.gradle"))
			exitCode, err = runCmd(cmd)
			return
		},
	},
	// The Grailsw builder will try to run the grailsw wrapper script to compile the project
	Builder{
		Name: "Grailsw",
		Detect: func(info os.FileInfo) bool {
			return info.Name() == "grailsw"
		},
		Configure: func(path string, static bool) error {
			return configureGraddleProject(path, static)
		},
		Build: func(projectPath string) (exitCode int, err error) {
			cmd := setupCmd(projectPath, exec.Command(path.Join(projectPath, "grailsw"), "compile"))
			exitCode, err = runCmd(cmd)
			return
		},
	},
}

func DefaultBuilder() Builder {
	// Try gradle by default, this should never happen.
	return Builders[1]
}
